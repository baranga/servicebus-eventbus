# eventbus
Wrapper for [servicebus](https://github.com/mateodelnorte/servicebus) with
event interface.

## Usage
```
const ServiceBus = require('servicebus');
const serviceBusRetry = require('servicebus-retry');
const EventBus = require('servicebus-eventbus');

let serviceBus = serviceBus.bus({
  url: '...',
  // don't create queues on sending events, consumer will do that
  assertQueuesOnFirstSend: false
});
serviceBus.use(serviceBusRetry());
let eventBus = new EventBus(serviceBus, 'someService');

eventBus.on('foo.bar', function (event, message) {
    console.log('foo had a bar');
});
eventBus.on('foo.baz', function (event, message) {
    console.log('foo had a baz');
});

eventBus.emit('foo.bar', {foo: 'bar'});
eventBus.emit('foo.baz', {foo: 'baz'});
```
