'use strict';

/**
 * Event orientated wrapper around service bus.
 * @constructor
 * @param {Object} serviceBus
 * @param {string} serviceName name of service
 */
function EventBus(serviceBus, serviceName) {
  this.bus = serviceBus;
  this.name = serviceName;
}

/**
 * Emit event on service bus.
 * @param {string} event
 * @param {*} payload
 */
EventBus.prototype.emit = function emit(event, payload) {
  this.bus.publish(event, payload, {routingKey: event});
};

/**
 * @callback EventBus~onCallback
 * @param {*} payload
 * @param {Object} payload.handle
 * @param {function()} payload.handle.ack
 * @param {function()} payload.handle.acknowledge
 * @param {function()} payload.handle.reject
 * @param {Object} message
 */

/**
 * Subscribe to event on service bus.
 * @param {string} event
 * @param {EventBus~onCallback} callback
 * @return {Object} subscription
 */
EventBus.prototype.on = function on(event, callback) {
  return this.bus.subscribe(
    `${this.name}.on(${event})`,
    {routingKey: event, ack: true},
    callback
  );
};

module.exports = EventBus;
